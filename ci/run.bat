Set dest=C:\GitLab-Runner\builds\1sbHHAxu\0\zv-tst\sd_st
Set source=C:\GitLab-Runner\builds\1sbHHAxu\0\zv-tst\sdst-cicd
Echo Your destination folder is: %dest%
Echo Your source folder is: %source%
IF [%dest%] == [] GOTO:eof
Set tag=%CI_COMMIT_TAG:~0,2%
Set repo=Repository doesnt exist
IF "%tag%"=="rc" Set repo=test
IF "%tag%"=="pr" Set repo=master
Echo All paths were implemented
Echo Deleting all files from %dest%
del %dest%\* /F /Q
@echo Files deleted.
Echo Deleting all folders from %dest%
for /d %%p in (%dest%\*) Do rd /Q /S "%%p"
@echo Folders deleted.
(robocopy %source% %dest% /E /XD .git) ^& IF %ERRORLEVEL% LSS 8 SET ERRORLEVEL = 0
cd %dest%
Echo checkout as %repo%
git checkout -B %repo%
Echo Commit and push into repo
git add .
git commit -m "%CI_COMMIT_MESSAGE%"
git push -u origin %repo%
@echo Push to GitLab is finished